class ArticlesController < ApplicationController
  def index
    @feed = Feed.find(params[:feed_id])
    @articles = @feed.articles
  end

  def show
    @article = Article.find(params[:id])
    @article.body # Initialize body content via model attribute getter override
  end
end
