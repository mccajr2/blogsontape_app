require 'rss'

class FeedsController < ApplicationController
  before_action :logged_in_user,     only: [:index, :destroy, :show]
  before_action :admin_user,     only: [:index, :destroy]

  def index
    @feeds = Feed.paginate(page: params[:page])
  end
  
  def show
    # For sidebar
    @subscription = current_user.subscriptions.build if logged_in?
    @feeds = current_user.feeds
    
    #For center panel
    @feed = Feed.find(params[:id])
    begin
      @rssFeed = RSS::Parser.parse(open(@feed.urlstring).read, false)
    rescue SocketError, OpenURI::HTTPError, SystemCallError => e
      flash[:danger] = "Unable to read RSS feed. Please check the URL and try again."
      redirect_to root_url
    end    
  end
  
  def autocomplete
    
    if params[:term]
      like  = "%".concat(params[:term].concat("%"))
      feeds = Feed.where("title like ?", like)
    else
      feeds = Feed.all
    end

    list = feeds.map {|u| Hash[id: u.id, label: u.title, name: u.title]}
    render json: list     
    
  end
  
  def destroy
    Feed.find(params[:id]).destroy
    flash[:success] = "Feed deleted"
    redirect_to feeds_url
  end    
  
end
