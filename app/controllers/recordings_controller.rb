require 'uri'
require 'open-uri'

class RecordingsController < ApplicationController
  include RecordingsHelper
 
def new
  @article = Article.find(params[:article_id])
  @recording = Recording.new
end
  
def create
  @article = Article.find(params[:article_id])
  @recording = @article.recordings.build(recording_params)
  @recording.uploaded_by = current_user.id
  @recording.uploaded_by_name = current_user.name
  if @recording.save
    flash[:success] = "Successfully added recording"
    redirect_to @recording
  else
    flash[:danger] = "Failed to save recording"
    get 'new'
  end
end

def create_tts
  @article = Article.find(params[:id])
  body = @article.body
  body = ActionController::Base.helpers.sanitize(body, :tags=>[])
  body = body.squish
  tts_text = URI.encode(body.to_str)
  tts_text = "http://tts-api.com/tts.mp3?q=#{tts_text}"
  @recording = @article.recordings.build(audiofile: URI.parse(tts_text))
  @recording.uploaded_by = tts_user.id
  @recording.uploaded_by_name = tts_user.name
  if @recording.save
    flash[:success] = "Successfully added recording"
    redirect_to @recording
  else
    flash[:danger] = "Failed to save recording"
    redirect_to :back
  end
end

def index
  @recordings = Recording.paginate(page: params[:page])
end

def show
  @recording = Recording.find(params[:id])
  @urlstring = @recording.audiofile.url # May be a coincidence, but audiojs doesn't find the url with erb. Send over as instance variable.
  @article_title = Article.find(@recording.article_id).title
  @uploaded_by = User.find(@recording.uploaded_by).name
end

def destroy
  Recording.find(params[:id]).destroy
  flash[:success] = "Recording deleted"
  redirect_to recordings_path
end

private

# Use strong_parameters for attribute whitelisting
# Be sure to update your create() and update() controller methods.

def recording_params
  params.require(:recording).permit(:audiofile)
end
  
end