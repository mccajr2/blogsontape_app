class StaticPagesController < ApplicationController
  
  def home
    @subscription = current_user.subscriptions.build if logged_in?
    @feeds = current_user.feeds if logged_in?
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
end
