require 'rss'

class SubscriptionsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def create
    @subscription = current_user.subscribe_by_url(subscription_params[:urlstring])
    if(@subscription.errors.empty?)
      flash[:success] = "Subscribed to RSS feed"
      feed = Feed.find(@subscription.feed_id)
      redirect_to feed_articles_path(feed)
    else
      flash[:danger] = "Unable to subscribe to RSS feed"
      redirect_to :back
    end
  end

  def destroy
  end
  
  private
  
    def subscription_params
      params.require(:subscription).permit(:urlstring)
    end
  
end
