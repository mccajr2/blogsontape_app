module ApplicationHelper
    
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Blogs on Tape"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end
  
  def current_user_feeds
    current_user.feeds if logged_in?
  end
    
  def subscribe_current_user
    current_user.subscriptions.build if logged_in?
  end
  
  def create_transient_article
    Article.new
  end
    
end
