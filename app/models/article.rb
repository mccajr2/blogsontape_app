require 'rubygems'
require 'nokogiri'

class Article < ActiveRecord::Base
  belongs_to :feed
  has_many :recordings
  
  before_destroy :check_for_recordings
  
  validates :urlstring, presence: true
  validates :title,     presence: true
  validates :preview,   presence: true  
  validates :feed_id,   presence: true
  
  # Override getter for body
  def body
    if self[:body].nil?
      parse_html_for_body
    end
    self[:body]
  end
  
  private
  
    def parse_html_for_body
      page = Nokogiri::HTML(open(self.urlstring), nil, 'UTF-8')
      page.xpath("//script").remove
      page.xpath("//img").remove
      page.xpath("//comment()").remove
    
      parent_keys = Array.new
      paragraphs = page.css('p')
      paragraphs.each do |p|
        if !!p.parent['id']
          parent_keys.push('id---' + p.parent['id'])
        end
        if !!p.parent['class']
          parent_keys.push('class---' + p.parent['class'])
        end
        if !!p.parent['style']
          parent_keys.push('style---' + p.parent['style'])
        end
      end
    
      parent_keys = parent_keys.uniq
      
      max_p = 0
      max_k = ""
      parent_keys.each do |k|
        h = k.split('---')
        path = "//div[@" + h[0] + "=\"" + h[1] + "\"]"
        elements = page.xpath(path)
        elements.each do |e|
          if e.xpath('./p').count > max_p
            max_p = e.xpath('./p').count
            max_k = k
          end
        end
      end
    
      max_h = max_k.split('---')
      max_path = "//div[@" + max_h[0] + "=\"" + max_h[1] + "\"]"
      
      update_attribute(:body, page.xpath(max_path).to_html.valid_encoding? ? page.xpath(max_path) : 'Encoding error')
    end
    
    def check_for_recordings
      if self.recordings.count > 0
        errors.add_to_base("cannot delete article while recordings exist")
        return false
      end      
    end

end
