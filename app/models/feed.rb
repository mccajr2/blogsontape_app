require 'rss'
class Feed < ActiveRecord::Base
  has_many :subscriptions,  dependent: :destroy
  has_many :articles,       dependent: :destroy
  has_many :users,          through: :subscriptions
  
  validates :urlstring, presence: true, uniqueness: { case_sensitive: true }
  validate :rss_feed_is_valid # Sets private attribute :title as well
  
  after_create :create_articles

  private
  
    def rss_feed_is_valid
      if self.urlstring.blank?
        return false
      end 
      
      begin
        @rss_feed = RSS::Parser.parse(open(self.urlstring).read, false)
        if !(self.title = @rss_feed.channel.title)
          self.title = self.urlstring   
        end
        
      rescue SocketError, OpenURI::HTTPError, SystemCallError => e
        errors.add(:urlstring, "is not a valid RSS URL")
      end 
    end
    
    def create_articles
      @rss_feed.items.each do |item|
        Article.create(feed_id: self.id, urlstring: item.link, 
          title: item.title, preview: item.description, pubdate: item.pubDate)       
      end
    end
    
    # Override private attribute setters
    
    def title=(val)
      write_attribute :title, val
    end

end