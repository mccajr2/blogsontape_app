class Recording < ActiveRecord::Base
  belongs_to :article
  belongs_to :user
  
  validates :article_id,    presence: true
  validates :uploaded_by,   presence: true

  has_attached_file :audiofile
  validates_attachment_presence :audiofile
  validates_attachment_content_type :audiofile, :content_type => [ 'audio/mpeg', 'audio/mp3', 'application/octet-stream' ],
                                    :message => 'file must be of filetype .mp3'
                                    
  before_save { self.audiofile_file_name = SecureRandom.hex(10) + self.audiofile_file_name }
                                    
end
