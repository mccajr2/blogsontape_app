class User < ActiveRecord::Base
  has_many :subscriptions, dependent: :destroy
  has_many :feeds, through: :subscriptions
  has_many :recordings, dependent: :destroy

  attr_accessor :remember_token
  before_save { self.email = self.email.downcase } 
  validates :name, presence: true, length: {maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, length: { minimum: 6 }, allow_blank: true
  
    # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end  
  
  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  # Subscribe to a feed
  def subscribe(feed)
    subscriptions.create(feed_id: feed.id)
  end
  
  def subscribe_by_url(urlstring)
    if (feed = Feed.find_by(urlstring: urlstring))
      subscription = subscribe(feed)
    else
      feed = feeds.create(urlstring: urlstring)
      subscription = subscriptions.find_by(feed_id: feed.id)
      if(subscription.nil? and feed.errors.count > 0)
        subscription = Subscription.new
        feed.errors.each do |attr,msg|
          subscription.errors[:base] << (feed[attr] + " " + msg)
        end
      end
    end
    subscription
  end
  
  # Unsubscribe to a feed
  def unsubscribe(feed)
    subscriptions.find_by(feed_id: feed.id).destroy
  end
  
  # Returns true if the current user is subscribed to the feed
  def subscribed?(feed)
    feeds.include?(feed)
  end
  
  
end
