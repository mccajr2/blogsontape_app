class CreateRecordings < ActiveRecord::Migration
  def change
    create_table :recordings do |t|
      t.integer :feed_id
      t.integer :uploaded_by
      t.string :urlstring
      t.binary :data
      t.string :filename
      t.string :content_type

      t.timestamps null: false
    end
    add_index :recordings, :u
    add_index :recordings, :feed_id
    add_index :recordings, [:uploaded_by, :feed_id], unique: true      
  end
end
