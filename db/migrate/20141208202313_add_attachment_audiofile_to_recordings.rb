class AddAttachmentAudiofileToRecordings < ActiveRecord::Migration
  def self.up
    change_table :recordings do |t|
      t.attachment :audiofile
    end
  end

  def self.down
    remove_attachment :recordings, :audiofile
  end
end
