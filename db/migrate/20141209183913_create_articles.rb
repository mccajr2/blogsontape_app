class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.references :feed, index: true
      t.integer :user_id
      t.string :title
      t.text :preview
      t.string :urlstring

      t.timestamps null: false
    end
  end
end
