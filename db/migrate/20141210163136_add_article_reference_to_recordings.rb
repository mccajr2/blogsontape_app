class AddArticleReferenceToRecordings < ActiveRecord::Migration
  def change
    add_reference :recordings, :article_id, index: true
  end
end
