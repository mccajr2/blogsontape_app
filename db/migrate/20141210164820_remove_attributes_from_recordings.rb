class RemoveAttributesFromRecordings < ActiveRecord::Migration
  def change
    remove_column :recordings, :data, :binary
    remove_column :recordings, :urlstring, :string
    remove_column :recordings, :filename, :string
    remove_column :recordings, :content_type, :string
  end
end
