class RemoveBadArticleIdFromRecordings < ActiveRecord::Migration
  def change
    remove_reference :recordings, :article_id, index: true
  end
end
