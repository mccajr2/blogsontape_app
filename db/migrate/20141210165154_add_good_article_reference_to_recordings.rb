class AddGoodArticleReferenceToRecordings < ActiveRecord::Migration
  def change
    add_reference :recordings, :article, index: true
  end
end
