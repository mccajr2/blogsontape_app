class AddPubDateToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :pubdate, :datetime
  end
end
