class AddUploadByNameToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :uploaded_by_name, :string
  end
end
