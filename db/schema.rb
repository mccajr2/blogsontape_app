# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141212210910) do

  create_table "articles", force: true do |t|
    t.integer  "feed_id"
    t.string   "title"
    t.text     "preview"
    t.string   "urlstring"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "body"
    t.datetime "pubdate"
  end

  add_index "articles", ["feed_id"], name: "index_articles_on_feed_id"

  create_table "feeds", force: true do |t|
    t.string   "urlstring"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flowers", force: true do |t|
    t.string   "color"
    t.string   "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recordings", force: true do |t|
    t.integer  "feed_id"
    t.integer  "uploaded_by"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "audiofile_file_name"
    t.string   "audiofile_content_type"
    t.integer  "audiofile_file_size"
    t.datetime "audiofile_updated_at"
    t.integer  "article_id"
    t.string   "uploaded_by_name"
  end

  add_index "recordings", ["article_id"], name: "index_recordings_on_article_id"
  add_index "recordings", ["feed_id"], name: "index_recordings_on_feed_id"
  add_index "recordings", ["uploaded_by", "feed_id"], name: "index_recordings_on_uploaded_by_and_feed_id", unique: true
  add_index "recordings", ["uploaded_by"], name: "index_recordings_on_uploaded_by"

  create_table "subscriptions", force: true do |t|
    t.string   "user_id"
    t.string   "feed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "subscriptions", ["feed_id"], name: "index_subscriptions_on_feed_id"
  add_index "subscriptions", ["user_id", "feed_id"], name: "index_subscriptions_on_user_id_and_feed_id", unique: true
  add_index "subscriptions", ["user_id"], name: "index_subscriptions_on_user_id"

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",           default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
