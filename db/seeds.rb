# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Users

User.create!(name:  "Generated TTS",
             email: "internaltts@example.com",
             password:              "internal-08df662c-a79f-4291-a631-7e84aca71047",
             password_confirmation: "internal-08df662c-a79f-4291-a631-7e84aca71047",             
             admin: true)

User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

# Feeds

#50.times do |n|
#  urlstring = Faker::Internet.url
#  title = Faker::Lorem.words(rand(2..10)).join(' ')
#  Feed.create!(urlstring: urlstring, 
#                    title: title)
#end

# Subscriptions

#r = Random.new

#User.all.each do |n|
#  c = r.rand(3..8)  
#  a = (1..50).to_a.sample c
#  a.each do |id|
#    n.subscriptions.create(feed_id: id)    
#  end
#end