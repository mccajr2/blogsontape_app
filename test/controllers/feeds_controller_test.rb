require 'test_helper'

class FeedsControllerTest < ActionController::TestCase

  def setup
    @admin = users(:michael)
    @non_admin = users(:archer)
    @feed = feeds(:cnnworld)
  end
  
  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end 
  
  test "should get index if logged in as admin" do
    log_in_as(@admin)
    get :index
    assert_response :success
  end  
  
  test "should redirect index when not logged in as admin" do
    log_in_as(@non_admin)
    get :index
    assert_redirected_to root_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Feed.count' do
      delete :destroy, id: @feed
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@non_admin)
    assert_no_difference 'Feed.count' do
      delete :destroy, id: @feed
    end
    assert_redirected_to root_url
  end  
  
  test "should redirect show if not logged in" do
    get :show, id: @admin
    assert_not flash.empty?
    assert_redirected_to login_url
  end 
  
  #test "should redirect show if not subscribed" do
  #  log_in_as(@admin)
  #  get :index
  #  assert_response :success
  #end  
  
  #test "should get show if logged in and subscribed" do
  #  log_in_as(@admin)
  #  get :index
  #  assert_response :success
  #end    

  #test "should redirect show if bad url" do
  #end
end
