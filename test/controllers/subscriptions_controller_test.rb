require 'test_helper'

class SubscriptionsControllerTest < ActionController::TestCase

  def setup
    @user = users(:michael)
    @subscription = subscriptions(:michaelcnnfeed)
  end

  #test "should redirect to feed show after create" do
  #  assert_difference 'Subscription.count' do
  #    post :create, @subscription
  #  end
  #  feed = Feed.find(@subscription.feed_id)
  #  assert_redirected_to feed_path(feed)
  #end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Subscription.count' do
      post :create, subscription: { }
    end
    assert_redirected_to login_url
  end  

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Subscription.count' do
      delete :destroy, id: @subscription
    end
    assert_redirected_to login_url
  end  

end
