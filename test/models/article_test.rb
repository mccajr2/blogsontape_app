require 'test_helper'

class ArticleTest < ActiveSupport::TestCase

  def setup
    @article = Article.new(urlstring: "http://example.com/article", 
                              title: "Example", preview: "Some text", feed_id: 1)    
  end
  
  test "should be valid" do
    assert @article.valid?
  end
  
  test "url should be required" do
    @article.urlstring = "   "
    assert_not @article.valid?
  end
  
  test "title should be required" do
    @article.title = "   "
    assert_not @article.valid?
  end
  
  test "preview should be required" do
    @article.preview = "   "
    assert_not @article.valid?
  end
  
  test "feed_id should be required" do
    @article.feed_id = "   "
    assert_not @article.valid?
  end
  

end
