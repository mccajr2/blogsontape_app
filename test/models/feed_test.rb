require 'test_helper'

class FeedTest < ActiveSupport::TestCase

  def setup
    @feed = feeds(:economist)    
  end
  
  test "should be valid" do
    assert @feed.valid?
  end
  
  test "url should be required" do
    @feed.urlstring = "   "
    assert_not @feed.valid?
  end
  
  test "url should be unique" do
    duplicate_feed = @feed.dup
    @feed.save
    assert_not duplicate_feed.valid?    
  end  
  
  test "url should be case sensitive" do
    duplicate_feed = @feed.dup
    duplicate_feed.urlstring = @feed.urlstring.upcase
    @feed.save
    assert duplicate_feed.valid?    
  end
  
  test "title should be populated before save" do
    assert @feed.title.nil?
    @feed.save
    assert_not @feed.title.nil?
  end
  
  test "title setter should be private, getter should be public" do
    @feed.save
    assert_raise(NoMethodError) do
      @feed.title = "Title"
    end
    assert_nothing_raised do
      assert @feed.title
    end
  end
  
end
